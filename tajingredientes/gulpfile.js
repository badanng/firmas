const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const gulp = require('gulp')
const sass = require('gulp-sass')
const browserySync = require('browser-sync')

// compilar scsss para el css
function style() {
    // 1.- donde se encuentran mis archivos .scss
    return gulp.src('./scss/**/*.scss')
    // 2.- compilar sass
    .pipe(sass().on('error', sass.logError)).pipe(plumber()) // si detecta errores continua la compilacion
    .pipe(sourcemaps.write())
    // 3.- donde guardo compilado css
    .pipe(gulp.dest('./css'))
    // 4.- escuchar cambios en el navegador
    .pipe(browserySync.stream())
}

function watch() {
    browserySync.init({
        server:{
            baseDir: './'
        }
    })   
    gulp.watch('./scss/**/*.scss', style) 
    gulp.watch('./**/*.html').on('change', browserySync.reload)
    gulp.watch('./js/**/*.js', style).on('change', browserySync.reload)

}

exports.style = style;
//exports.watch = watch;
exports.default = watch;